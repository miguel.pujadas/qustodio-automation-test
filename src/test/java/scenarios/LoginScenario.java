package scenarios;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import tools.Waits;
import pages.HeaderPage;

import static pages.LoginPage.pageFactory;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginScenario {
    public void login_with_wrong_credentials(WebDriver driver, String user, String password) throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        driver.get("https://family.qustodio.com/");
        loginPage.login(driver,user,password);
        Waits.waitForElementToBeDisplayed(driver, pageFactory.getErrorWrongCredentials(), 3);
    }
    public void login_with_good_credentials(WebDriver driver, String user, String password) throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        HeaderPage headerPage = new HeaderPage(driver);
        driver.get("https://family.qustodio.com/");
        loginPage.login(driver,user,password);
        headerPage.amIIn(driver);
    }
}
