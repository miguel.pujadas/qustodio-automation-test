package scenarios;

import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import pages.HeaderPage;

import java.awt.*;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class HeaderScenario {
    public void logout(WebDriver driver, String user, String password) throws InterruptedException, AWTException {
        LoginPage loginPage = new LoginPage(driver);
        HeaderPage headerPage =  new HeaderPage(driver);
        driver.get("https://family.qustodio.com/");
        loginPage.login(driver,user,password);
        headerPage.logout(driver, user, password);
    }
}
