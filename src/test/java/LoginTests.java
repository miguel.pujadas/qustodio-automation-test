import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scenarios.LoginScenario;
import java.net.MalformedURLException;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginTests extends LoginScenario {
    public static final String user = System.getProperty("user");
    public static final String password = System.getProperty("password");
    private static WebDriver driver;
    @BeforeMethod(alwaysRun = true)
    public void setup() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        driver = new ChromeDriver(capability);
        driver.manage().window().maximize();
    }
    @Test (groups = { "wrong_credentials" })
    public void loginWrongCredentials() throws Exception {
        // You can send a custom user and password by Maven: mvn test -DLoginTests -Duser=”user” -Dpassword=”password”
        login_with_wrong_credentials(driver,user,password);
    }
    @Test (groups = { "correct_credentials" })
    public void loginGoodCredentials() throws Exception {
        // You can send a custom user and password by Maven: mvn test -DLoginTests -Duser=”user” -Dpassword=”password”
        login_with_good_credentials(driver,user,password);
    }
    @AfterMethod(alwaysRun = true)
    public void teardown() {
        driver.quit();
    }
}
