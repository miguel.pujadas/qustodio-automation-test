package pages;
import org.openqa.selenium.WebDriver;
import details.LoginDetails;
import factories.LoginFactory;
import tools.Waits;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginPage {
    public static LoginDetails pageFactory;
    public LoginPage(WebDriver driver){
        this.pageFactory = new LoginDetails(driver);
    }

    public static void login(WebDriver driver, String user, String password){
        Waits waits = new Waits();
        LoginDetails loginDetails = LoginFactory.createLoginData(user, password);
        waits.waitForElementToBeDisplayed(driver, pageFactory.getUserInput(),5);
        pageFactory.getUserInput().sendKeys(loginDetails.getUser());
        waits.waitForElementToBeDisplayed(driver, pageFactory.getUserPasswordInput(),5);
        pageFactory.getUserPasswordInput().sendKeys(loginDetails.getPassword());
        waits.waitForElementToBeDisplayed(driver, pageFactory.getLoginButton(),5);
        pageFactory.getLoginButton().click();
    }
}
