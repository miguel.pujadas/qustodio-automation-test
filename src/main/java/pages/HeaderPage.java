package pages;
import org.openqa.selenium.WebDriver;
import details.HeaderDetails;
import tools.Waits;
import java.awt.*;


/**
 * Created by Miguel Pujadas Palenzuela
 */
public class HeaderPage {
    public static HeaderDetails pageFactory;
    public HeaderPage(WebDriver driver){
        this.pageFactory = new HeaderDetails(driver);
    }

    public static void logout(WebDriver driver, String user, String password) throws AWTException, InterruptedException {
        Waits.waitForElementToBeClickable(driver, pageFactory.getLogoutButton() , 10);
        pageFactory.getLogoutButton().click();
    }

    public static void amIIn(WebDriver driver){
        Waits.waitForElementToBeDisplayed(driver, pageFactory.getLogoutButton(), 5);
    }
}
