package factories;
import details.LoginDetails;
import implementations.LoginImplementation;
/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginFactory {
    public static LoginDetails createLoginData(){
        return new LoginImplementation();
    }
    public static LoginDetails createLoginData(String user, String password){
        return new LoginImplementation(user, password);
    }
}
