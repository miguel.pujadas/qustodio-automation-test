package factories;

import details.HeaderDetails;
import implementations.HeaderImplementation;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class HeaderFactory {
    public static HeaderDetails createHeaderData(){
        return new HeaderImplementation();
    }
    }

