package constants;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginConstants {
    public static final String USER_CSS = "[name=email]";
    public static final String PASSWORD_CSS = "[name=password]";
    public static final String LOGIN_BUTTON_ID= "login_button";
    public static final String ERROR_WRONG_CREDENTIALS_CSS = "[class=error]";
}
