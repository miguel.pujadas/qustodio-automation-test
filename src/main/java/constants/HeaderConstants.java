package constants;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class HeaderConstants {
    public static final String LOGOUT_BUTTON_ID = "logout_button";
    public static final String MODAL_CLASS = "Modal__text";
}
