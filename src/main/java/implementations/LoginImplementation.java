package implementations;
import details.LoginDetails;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginImplementation extends LoginDetails{
    public LoginImplementation(){
        super();
        setUser("richardcbeeks@rhyta.com");
        setPassword("123456");
    }

    public LoginImplementation(String user, String password){
        super();
        if(user!=null)setUser(user);
        else
            setUser("richardcbeeks@rhyta.com");
        if(password!=null)setPassword(password);
        else
            setPassword("123456");
    }
}