package details;

import constants.HeaderConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class HeaderDetails extends HeaderConstants {
    public String user;
    public String password;
    public HeaderDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public HeaderDetails(){}

    @FindBy(id = HeaderConstants.LOGOUT_BUTTON_ID)
    private WebElement logoutButton;
    public WebElement getLogoutButton() {return logoutButton;}

    @FindBy(className = HeaderConstants.MODAL_CLASS)
    private WebElement modal;
    public WebElement getModal() {return modal;}

}
