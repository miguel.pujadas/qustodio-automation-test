
package details;

import constants.LoginConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginDetails extends LoginConstants{
    public String user;
    public String password;
    public LoginDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public LoginDetails(){}

    @FindBy(css = LoginConstants.USER_CSS)
    private WebElement userInput;
    public WebElement getUserInput() {return userInput;}
    public void setUser(String user) {this.user = user;}
    public String getUser(){return user;}

    @FindBy(css = LoginConstants.PASSWORD_CSS)
    private WebElement passwordInput;
    public WebElement getUserPasswordInput() {return passwordInput;}
    public void setPassword(String password) {this.password = password;}
    public String getPassword(){return password;}

    @FindBy(id = LoginConstants.LOGIN_BUTTON_ID)
    private WebElement loginButton;
    public WebElement getLoginButton() {return loginButton;}

    @FindBy(css = LoginConstants.ERROR_WRONG_CREDENTIALS_CSS)
    private WebElement errorWrongCredentials;
    public WebElement getErrorWrongCredentials() {return errorWrongCredentials;}
}
